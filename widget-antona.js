widgetNameIntr = function() {
    var widget = this;
    this.code = null;

    this.yourVar = {};
    this.yourFunc = function() {};

    // вызывается один раз при инициализации виджета, в этой функции мы вешаем события на $(document)
    this.bind_actions = function(){
        //пример $(document).on('click', 'selector', function(){});
    };

    // вызывается каждый раз при переходе на страницу
    this.render = function() {
		
    };

    // вызывается один раз при инициализации виджета, в этой функции мы загружаем нужные данные, стили и.т.п
    this.init = function(){
    	var colorText = 'red';
		//элемент родитель с айди 
		var elMain = document.getElementById('pipeline_holder');
		//третий элемент который нужно выкрасить
		var el = elMain.childNodes[0].childNodes[0].childNodes[2].childNodes[0].childNodes[0];
		// первая строка с названием
		elColor1 = el.childNodes[0];
		elColor1.style.color = colorText;
		//вторая строка с кол-во сделок
		elColor2 = el.childNodes[1].childNodes[0].childNodes;
		//перебираем массив дочерних элементов у нижней строки и красим текст в красный
		for (i=0; i < elColor2.length; i++){
		    forEl = elColor2[i];
		    forEl.style.color = colorText;
		}
    };

    // метод загрузчик, не изменяется
    this.bootstrap = function(code) {
        widget.code = code;
        // если frontend_status не задан, то считаем что виджет выключен
        // var status = yadroFunctions.getSettings(code).frontend_status;
        var status = true;

        if (status) {
            widget.init();
            widget.render();
            widget.bind_actions();
            $(document).on('widgets:load', function () {
                widget.render();
            });
        }
    }
};
// создание экземпляра виджета и регистрация в системных переменных Yadra
// widget-name - ИД и widgetNameIntr - уникальные названия виджета
yadroWidget.widgets['widget-antona'] = new widgetNameIntr();
yadroWidget.widgets['widget-antona'].bootstrap('widget-antona');
